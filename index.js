const {XcApis} = require("xc-instant");
require('dotenv').config();
const app = require('express')();

if (!process.env.XC_SERVERLESS_TYPE) {
  (async () => {
    app.use(await (new XcApis()).init());
    app.listen(process.env.PORT || 8080);
    console.log(`App started successfully.\nVisit -> http://localhost:${process.env.PORT || 8080}/xc`);
  })().catch(e => console.log(e))
  module.exports = app;
} else {
  module.exports.xc_function = new XcApis().makeServerless(app, process.env.XC_SERVERLESS_TYPE);

// for serverless lambda set XC_SERVERLESS_TYPE=SERVERLESS


// for Azure use below
// module.exports = new XcApis().makeServerless(serverless, process.env.XC_SERVERLESS_TYPE);

// module.exports = new XcApis().makeServerless(serverless, 'AZURE_FUNCTION_APP');
// module.exports.handler = new XcApis().makeServerless(serverless, 'GCP_FUNCTION');
// module.exports.ali = new XcApis().makeServerless(serverless, 'ALIYUN');
// module.exports.zeit = new XcApis().makeServerless(serverless, 'ZEIT');
// module.exports.lambda = new XcApis().makeServerless(serverless, 'AWS_LAMBDA');
}
